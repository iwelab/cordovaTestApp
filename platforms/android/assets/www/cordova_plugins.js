cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/cordova-plugin-android-permissions/www/permissions.js",
        "id": "cordova-plugin-android-permissions.Permissions",
        "clobbers": [
            "window.plugins.permissions"
        ]
    },
    {
        "file": "plugins/cordova-plugin-photo-engine/www/js/photo-engine.js",
        "id": "cordova-plugin-photo-engine.PhotoEngine",
        "clobbers": [
            "PhotoEngine"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.2.2",
    "android.support.v4": "21.0.1",
    "cordova-plugin-android-permissions": "0.6.0",
    "cordova-plugin-photo-engine": "1.0.0"
};
// BOTTOM OF METADATA
});