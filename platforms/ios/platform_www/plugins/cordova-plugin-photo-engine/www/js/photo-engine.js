cordova.define("cordova-plugin-photo-engine.PhotoEngine", function(require, exports, module) {
//
//  photo-engine.js
//

function PhotoEngine() {}

PhotoEngine.prototype.photoList = function(callback) {
  cordova.exec(function(result) { callback(result); }, function(error) { alert(error); }, "PhotoEngine", "photoList", []);
}

PhotoEngine.prototype.storePhoto = function(id, callback) {
  cordova.exec(function(result) { callback(result); }, function(error) { alert(error); }, "PhotoEngine", "storePhoto", [id]);
}

module.exports = new PhotoEngine();

});
