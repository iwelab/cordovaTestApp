setTimeout(function() {
  var permissions = window.plugins.permissions;
  permissions.hasPermission(checkReadPermissionCallback, null, permissions.READ_EXTERNAL_STORAGE);
  permissions.hasPermission(checkWritePermissionCallback, null, permissions.WRITE_EXTERNAL_STORAGE);

  function checkReadPermissionCallback(status) {
    if(!status.hasPermission) {
      var errorCallback = function() {
        console.warn('permission is not turned on');
      }

      permissions.requestPermission(function(status) {
        if( !status.hasPermission ) errorCallback();
      }, errorCallback, permissions.READ_EXTERNAL_STORAGE);
    }
  }

  function checkWritePermissionCallback(status) {
    if(!status.hasPermission) {
      var errorCallback = function() {
        console.warn('permission is not turned on');
      }

      permissions.requestPermission(function(status) {
        if( !status.hasPermission ) errorCallback();
      }, errorCallback, permissions.WRITE_EXTERNAL_STORAGE);
    }
  }
}, 3000);

setTimeout(function() {
  PhotoEngine.photoList(function(result) { 
    alert(JSON.stringify(result.data));
  });
}, 3000);

setTimeout(function() {
  PhotoEngine.storePhoto(0, function(result) {
    alert(result.data);
  });
}, 6000);
