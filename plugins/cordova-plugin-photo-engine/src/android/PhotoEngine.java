package com.iwelab.cordova.photoengine;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.provider.MediaStore;
import android.util.Log;
import android.database.Cursor;
import android.net.Uri;
import android.media.ExifInterface;
import android.os.Bundle;
import android.graphics.Bitmap; 
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;

import java.io.*;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat; 
import java.text.ParseException;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

public class PhotoEngine extends CordovaPlugin {
	private final String ACTION_PHOTOLIST = "photoList";
	private final String ACTION_STOREPHOTO = "storePhoto";
	JSONArray jsonArray;

	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		if (action.equals(ACTION_PHOTOLIST)) {
			this.photoList(callbackContext);
		}

		if (action.equals(ACTION_STOREPHOTO)) {
			String photoIndex = args.getString(0);
			this.storePhoto(photoIndex, callbackContext);
			return true;
		}

		return false;
	}

	private void photoList(CallbackContext callbackContext) {
		int index = 0;
		Uri uri;
		Cursor cursor;
		int column_index_data, column_index_folder_name;
		String absolutePathOfImage = null;

		jsonArray = new JSONArray();

		uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

		String[] projection = { MediaStore.MediaColumns.DATA,
						MediaStore.Images.Media.BUCKET_DISPLAY_NAME };

		cursor = this.cordova.getActivity().getContentResolver().query(uri, projection, null,
						null, null);

		if (cursor != null) {
			column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
			column_index_folder_name = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);

			while (cursor.moveToNext()) {
				absolutePathOfImage = cursor.getString(column_index_data);

				String ext = absolutePathOfImage.substring(absolutePathOfImage.lastIndexOf('.') + 1, absolutePathOfImage.length());
				if (! ext.equals("JPG") && ! ext.equals("jpg"))
					continue;

				String dateTime = "";
				long timestamp = 0;
				String lat = "";
				String lon = "";

				try {
					ExifInterface exif = new ExifInterface(absolutePathOfImage);
					dateTime = exif.getAttribute(ExifInterface.TAG_DATETIME);

//					SimpleDateFormat formatter = new SimpleDateFormat("yyyy:MM:dd hh:mm:ss");
//					try {
//						Date date = (Date)formatter.parse(dateTime);
//						timestamp = date.getTime();
//					} catch (ParseException e) {}
					

					GeoDegree geoDegree = new GeoDegree(exif);
					lat = geoDegree.getLatitude();
					lon = geoDegree.getLongitude();

				} catch (IOException e) {}

				if (lat == "null" || lon == "null")
					continue;

				JSONObject obj = new JSONObject();
				try {
					obj.put("id", index++);
					obj.put("url", absolutePathOfImage);
					obj.put("timestamp", dateTime);
					obj.put("latitude", lat);
					obj.put("longitude", lon);
					jsonArray.put(obj);
				} catch(JSONException e) {}
			}

			try {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("error", 0);
				jsonObject.put("data", jsonArray);

				callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, jsonObject));
				callbackContext.success(jsonObject);
			} catch(JSONException e) {}

		} else {
				JSONObject json = new JSONObject();
				try {
					json.put("error", 900);
					json.put("data", "no photo founded");

					callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, json));
					callbackContext.success(json);
				} catch(JSONException e) {}
		}

	}

	private void storePhoto(String photoIndex, CallbackContext callbackContext) {
		if (photoIndex != null && photoIndex.length() > 0) {
			JSONObject photoData = new JSONObject();
			try {
				photoData = jsonArray.getJSONObject(Integer.parseInt(photoIndex));
			} catch(JSONException e) {}

			String url = null;
			try {
				url = photoData.getString("url");
			} catch(JSONException e) {}

			Bitmap srcBmp = BitmapFactory.decodeFile(url);
			int iWidth = 1024;
			int iHeight = 1024;
			float fWidth = srcBmp.getWidth();
			float fHeight = srcBmp.getHeight();

			if(fWidth > iWidth) {
				float mWidth = (float) (fWidth / 100);
				float fScale = (float) (iWidth / mWidth);
				fWidth *= (fScale / 100);
				fHeight *= (fScale / 100);
			} else if (fHeight > iHeight) {
				float mHeight = (float) (fHeight / 100);
				float fScale = (float) (iHeight / mHeight);
				fWidth *= (fScale / 100);
				fHeight *= (fScale / 100);
			}

			String path = url.substring(0, url.lastIndexOf("/") + 1);
			String file = url.substring(url.lastIndexOf("/") + 1);
			String newUrl = path + "_" + file;

			FileOutputStream fileObj = null;
			try {
				fileObj = new FileOutputStream(newUrl);
				Bitmap resizedBmp = Bitmap.createScaledBitmap(srcBmp, (int)fWidth, (int)fHeight, true);
				resizedBmp.compress(CompressFormat.JPEG, 80, fileObj);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				close(fileObj);
			}

			JSONObject json = new JSONObject();
			try {
				json.put("error", 0);
				json.put("data", newUrl);

				callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, json));
				callbackContext.success(json);
			} catch(JSONException e) {}
		} else {
			JSONObject json = new JSONObject();
	 		try {
				json.put("error", 0);
				json.put("data", "no photo founded");

				callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, json));
				callbackContext.success(json);
			} catch(JSONException e) {}

		}
	}

	public static void close(Closeable c) {
		if (c == null) return; 
		try {
			c.close();
		} catch (IOException e) {
			//log the exception
		}
	}
}
